---
title: "Insurance customer retention"
author: "Alexander Matrunich"
date: "7/28/2019"
output: 
  html_document:
    code_folding: hide
    toc: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r library, echo=FALSE, include=FALSE}
library("purrr")
library("stringr")
library("ggplot2")
library("visdat")
library("lubridate")
library("tidyr")
# library("skimr")
library("dplyr")
library("ggmap")
library("magrittr")
```


```{r data, cache=FALSE, echo=FALSE, include=FALSE}
ds <- readRDS(file.path("data", "insurance.rds"))

data <- with(ds, {
  communities %>% 
    left_join(customers %>% 
                rename(client_created_at = created_at,
                       client_updated_at = updated_at), by = "community_id") %>% 
    left_join(customer_policies %>% 
                rename(policy_created_at = created_at,
                       policy_updated_at = updated_at), by = "customer_id") %>% 
    left_join(policy_transactions %>% 
                rename(transaction_created_at = created_at,
                       transaction_updated_at = updated_at), by = "customer_policy_id") 
})
```


## Introduction

The document contains the log of insurance datesets exploring. There were two main goals: (1) to get the feel of the data; (2) to understand how to measure the retention. Results of some calculations are available in an interactive Shiny dashboard.

All procedures were conducted using R language. The source code is available in a Gitlab repository: [https://gitlab.com/Matrunich/insurance_customers_retention](https://gitlab.com/Matrunich/insurance_customers_retention). The Shiny dashboard is published online at the Shinyapps.io server: [https://malexan.shinyapps.io/customer_retention/](https://malexan.shinyapps.io/customer_retention/). This RMarkdown document itself in the knitted form is provided through the RPubs service: [http://rpubs.com/malexan/retention](http://rpubs.com/malexan/retention).

## The structure of datasets

```{r datvis}
ds %>% 
  iwalk(~{print(vis_dat(.x, warn_large_data = FALSE) + ggtitle(.y))
    print(summary(.x))
    glimpse(.x)
    # print(skim(.x))
    })
```


### Missing values

Up to 1000 random observations per set. 

```{r missings}
ds %>% 
  iwalk(~ print(vis_miss(
    if(nrow(.x) > 1000L)  
      sample_n(.x, 1000L) else .x,
    cluster = TRUE,
    sort_miss = TRUE,
    warn_large_data = FALSE) + ggtitle(.y)))

```

### Date planted is in the future

```{r}
ds$customer_policies %>% 
  filter(date_planted > today())
```

What does it mean? Good example to apply `validate` package.

### Correspondence between season and other date columns

Let's check does season match other periods.


```{r}
ds$customers %>% 
  select(customer_id) %>% 
  sample_n(1) %>% 
  left_join(data %>% 
              mutate_if(is.POSIXt, as.Date) %>% 
              select_if(map_lgl(., is.Date) | names(.) %in% c(
                "customer_id",
                "policy_transaction_id", 
                "customer_policy_id")), 
            by = 'customer_id') %>% 
  gather("event", "date", -ends_with("_id")) %>% 
  arrange(date)
```

### Maximum number of records (transactions) per customer

```{r}
data %>% 
  filter(!is.na(customer_id)) %>% 
  group_by(customer_id) %>% 
  summarize(records = n()) %>%
  top_n(5, records)
```

What is happening with one of champions?

```{r}
data %>% 
  filter(customer_id == 172600) %>% 
  mutate_if(is.POSIXt, as.Date) %>% 
  select_if(map_lgl(., is.Date) | names(.) %in% c(
    "customer_id",
    "policy_transaction_id", 
    "customer_policy_id"))
```

... only columns with differences. 

```{r}
data %>% 
  filter(customer_id == 172600) %>% 
  mutate_if(is.POSIXt, as.Date) %>% 
  select_if(~length(unique(.)) > 1L)
```

Based on the provided documentation we can work with `transaction_date`, so `created_at` and `updated_at` could be dropped. 

So each customer could have only one policy, right?

```{r}
data %>% 
  select(customer_id, customer_policy_id) %>% 
  distinct() %>% 
  group_by(customer_id) %>% 
  summarize(policies = n()) %>% 
  top_n(5, policies)
```


~~Right.~~ No, up to six policies per client in our set. 

Transactions?

```{r}
data %>% 
  # filter(customer_id == 172600) %>% 
  select(customer_id, customer_policy_id, policy_transaction_id) %>% 
  distinct() %>% 
  group_by(customer_id, customer_policy_id) %>% 
  summarize(transactions = n()) %>% 
  ungroup() %>% 
  top_n(5, transactions) %>% 
  arrange(desc(transactions))
```

Up to twelve payments from a client per one policy.


Time range. One policy.

```{r}
data %>% 
  filter(customer_id == 172600) %>% 
  select(registration_date, customer_policy_id, date_issued, policy_transaction_id, transaction_date, transaction_amount) %>% 
  arrange(date_issued, transaction_date)
```

Time range for multiple policies.


```{r}
data %>% 
  filter(customer_id == 117) %>% 
  select(registration_date, customer_policy_id, date_issued, policy_transaction_id, transaction_date, transaction_amount) %>% 
  arrange(date_issued, transaction_date)
```

The 117 is a loyal customer!

Are there any policies covering more than season/year?

```{r}
policy_years <- data %>% 
  mutate(transaction_year = year(transaction_date)) %>% 
  mutate(season_year = as.integer(str_extract(season, "^\\d{4}")),
         season_type = str_remove(season, "^\\d{4} ")) %>% 
  select(customer_policy_id, ends_with("year"), starts_with("season")) %>% 
  distinct() %>% 
  glimpse()

policy_years %>% 
  group_by(customer_policy_id) %>% 
  summarise(trans_years = length(unique(transaction_year))) %>% 
  arrange(desc(trans_years)) %>% 
  head()

policy_years %>% 
  group_by(customer_policy_id) %>% 
  summarise(season_years = length(unique(season_year))) %>% 
  arrange(desc(season_years)) %>% 
  head()

policy_years %>% 
  group_by(customer_policy_id) %>% 
  summarise(seasons = length(unique(season))) %>% 
  arrange(desc(seasons)) %>% 
  head()

policy_years %>% 
  group_by(customer_policy_id) %>% 
  summarise(season_types = length(unique(season_type))) %>% 
  arrange(desc(season_types)) %>% 
  head()
```

OK, it is true: one policy per one season :)

### More time/date columns 

Let's check relations between various date/time columns.

```{r}
ds$community_payouts %>% 
  ggplot(aes(paid_date, created_at)) +
  geom_point(alpha = .3) + 
  ggtitle("Community payouts")

ds$customer_policies %>% 
  ggplot(aes(date_issued, created_at)) +
  geom_point(alpha = .3) + 
  ggtitle("Customer policies")

ds$customers %>% 
  ggplot(aes(registration_date, created_at)) +
  geom_point(alpha = .3) + 
  ggtitle("Customers")

ds$customers %>% 
  ggplot(aes(updated_at, created_at)) +
  geom_point(alpha = .3) + 
  ggtitle("Customers updated")

ds$policy_transactions %>% 
  ggplot(aes(transaction_date, created_at)) +
  geom_point(alpha = .3) + 
  ggtitle("Transactions")

```


All columns like `created_at` look more technical, we can remove them. Let's update the dataset using latest findings.

```{r}
data <- data %>% 
  mutate(season_year = as.integer(str_extract(season, "^\\d{4}")),
         season_type = str_remove(season, "^\\d{4} ")) %>% 
  select(
    -country,
    -iso_code,
    -policy_created_at,
    -client_created_at,
    -transaction_created_at
  )
```


### Minor / major seasons

```{r}
data %>%
  select(customer_policy_id, season_type, date_issued) %>% 
  distinct() %>% 
  ggplot(aes(date_issued, fill = season_type)) + 
  geom_density(alpha = .3) +
  ggtitle("Do major and minor seasons not overlap across country?")
```

In the whole sample major/minor season overlap, probably they vary across crops.

```{r}
data %>%
  select(crop, customer_policy_id, season_type, date_issued) %>% 
  filter(!is.na(crop)) %>% 
  distinct() %>% 
  ggplot(aes(date_issued, fill = season_type)) + 
  geom_density(alpha = .3) + 
  facet_wrap(~crop, ncol = 1L, scales = "free_y") + 
  labs(x = NULL, y = NULL,
       title = "Minor and major seasons for crops")
```

Maybe regions?

```{r}
data %>%
  select(region, customer_policy_id, season_type, date_issued) %>% 
  distinct() %>% 
  ggplot(aes(date_issued, fill = season_type)) + 
  geom_density(alpha = .3) + 
  facet_wrap(~region, ncol = 1L, scales = "free_y") + 
  labs(x = NULL, y = NULL,
       title = "Can minor and major seasons overlap inside of a region?")
```

In GH-TV people are watching wide-screen TV. Right, there were 4 payments only. But even on the level of a region major and minor seasons can overlap. Let's take one region with both types of season and split by crop.

```{r}
data %>%
  filter(region == "GH-BA",
         !is.na(crop)) %>% 
  select(crop, customer_policy_id, season_type, date_issued) %>% 
  distinct() %>% 
  ggplot(aes(date_issued, fill = season_type)) + 
  geom_density(alpha = .3) + 
  facet_wrap(~crop, ncol = 1L, scales = "free_y") + 
  labs(title = "GH-BA by crop", x = NULL, y = NULL)
```

... still overlapping. The level of district!

```{r}
data %>%
  filter(region == "GH-BA",
         !is.na(crop)) %>% 
  select(crop, district, customer_policy_id, season_type, date_issued) %>% 
  distinct() %>% 
  ggplot(aes(date_issued, fill = season_type)) + 
  geom_density(alpha = .3) + 
  facet_wrap(~district, ncol = 1L, scales = "free_y") + 
  labs(title = "GH-BA", x = NULL, y = NULL)
```

```{r}
data %>%
  filter(district %in% c("Tano North", "Sunyani West"),
         !is.na(crop)) %>% 
  select(crop, district, customer_policy_id, season_type, date_issued) %>% 
  distinct() %>% 
  ggplot(aes(date_issued, fill = season_type)) + 
  geom_density(alpha = .3) + 
  facet_wrap(~district + crop, ncol = 1L, scales = "free_y") + 
  labs(title = "GH-BA districts by crop", x = NULL, y = NULL)
```

```{r, fig.height = 10}
data %>%
  filter(district == "Sunyani West") %>% 
  select(community_name, customer_policy_id, season_type, date_issued) %>% 
  distinct() %>% 
  ggplot(aes(date_issued, fill = season_type)) + 
  geom_density(alpha = .3) + 
  facet_wrap(~community_name, ncol = 1L, scales = "free_y") + 
  labs(title = "GH-BA, Sunyani West communities", x = NULL, y = NULL)
```

OK, on the level of communities season types are stable. So to have proper mapping of seasons to calendar we need community-level scheme. Drop it!


### Spatial distributions

Spatial distribution of communities.

```{r}
ds$communities %>% 
  filter(!is.na(latitude)) %>% 
  ggplot(aes(latitude, longitude)) + 
  geom_point() + 
  coord_map()
```

Garbage happens: some mistakes in the data, probably. Applying some filtering... 

```{r}
ds$communities %>% 
  filter(!is.na(latitude)) %>% 
  filter(latitude < 9) %>% 
  ggplot(aes(latitude, longitude)) + 
  geom_point() + 
  coord_map() +
  ggtitle("It doesn't look like Ghana",
          "And didn't I messed axises up?")
```

OK, we've got Ghana coordinate box from OpenStreetMap. 

```{r getmap, message=FALSE, echo=TRUE, cache=TRUE}
ghana <- c(left = -3.2, top = 11.5, right = 1.4, bottom = 4.7)
ghana_back <- get_stamenmap(ghana, zoom = 8, maptype = "terrain-background") 
# toner-lite or toner-backgroud are also OK
```

```{r}
ghana_back %>% 
  ggmap() +
  geom_point(
    aes(longitude, latitude), 
    data = ds$communities %>% filter(!is.na(longitude)),
    alpha = .2, color = "purple") + 
    theme_void()
  
```

Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.


```{r north_ghana, message=FALSE, echo=TRUE, cache=TRUE}
ghana_north_bbox <- c(left = -3.2, top = 11.5, right = 1.4, bottom = 6.8)
ghana_north <- get_stamenmap(ghana_north_bbox, 
                             zoom = 9, 
                             maptype = "toner-background") 
# toner-lite or toner-backgroud are also OK
ghana_north_small <- get_stamenmap(ghana_north_bbox, 
                             zoom = 8, 
                             maptype = "toner-background") 

saveRDS(ghana_north, file.path("retentionapp", "data", "map_ghana_north.rds"))
```

### Communities by features

```{r}
data %>% 
  select(customer_id, 
         longitude, 
         latitude) %>%
  filter(!is.na(latitude), 
         !is.na(longitude),
         !is.na(customer_id)) %>% 
  distinct() %>% 
  {ghana_north %>% 
      ggmap() +
      stat_density_2d(
        aes(longitude, latitude, fill = ..level..), geom = "polygon", alpha = .3, 
        data = .,) + 
      theme_void() +
      guides(fill = FALSE) + 
      ggtitle("Density of customers' locations")
        } %>% print  
```


```{r}
data %>% 
  select(customer_id, 
         longitude, 
         latitude, 
         season) %>%
  na.omit() %>% 
  distinct() %>% 
  {ghana_north_small %>% 
      ggmap() +
      stat_density_2d(
        aes(longitude, latitude, fill = ..level..), geom = "polygon", alpha = .3, 
        data = .,) + 
      theme_void() +
      guides(fill = FALSE) + 
      ggtitle("Density of customers' locations by seasons") + 
      facet_wrap(~season)
        } %>% print  
```

```{r}
data %>% 
  select(customer_id, 
         longitude, 
         latitude, 
         crop) %>%
  na.omit() %>% 
  distinct() %>% 
  {ghana_north %>% 
      ggmap() +
      # stat_density_2d(
      #   aes(longitude, latitude, fill = ..level..), geom = "polygon", alpha = .3, 
      #   data = .,) + 
      geom_density_2d(aes(longitude, latitude, color = crop), data = .) + 
      theme_void() +
      guides(fill = FALSE) + 
      ggtitle("Density of customers' locations by crops")  
        } %>% print  
```


## Retention

From the project description we have two slightly different definitions of retention: 

> we think of a “retained” customer as a customer who purchased drought insurance in
multiple cropping seasons.
>
> *retention rate* Percentage of prior season customers who purchase insurance again in a subsequent season.

The first variant with multiple seasons is easy. In the second one the added complication is that it is not straightforward how to define "subsequent" season as for various communities and crops minor and major seasons are possible. Additionally we have only four years including incomplete. Let's stick to the first definition for now.

We assume a policy is issued around the beginning of a season.

```{r}
data %>% 
  select(customer_policy_id, date_issued) %>% 
  na.omit() %>% 
  distinct() %>% 
  ggplot(aes(customer_policy_id, date_issued)) + 
  geom_point(alpha = .2) + 
  ggtitle("Policy ID can't be used as ordered substitution for date issued")
```

Cohort analysis, as Google Analytics calls it. By month of first policy.

```{r}
data %>% 
  select(
    customer_id,
    customer_policy_id,
    date_issued
  ) %>% 
  distinct() %>% 
  group_by(customer_id) %>% 
  mutate(
    month_issued = ceiling_date(date_issued, 'month'),
    queue = dense_rank(month_issued),
    first_policy = min(month_issued)
  ) %>% 
  group_by(first_policy, queue) %>% 
  summarise(policies = n()) %>% 
  ungroup() %>% 
  spread(queue, policies)
```

```{r}
data %>% 
  select(
    customer_id,
    customer_policy_id,
    date_issued
  ) %>% 
  distinct() %>% 
  group_by(customer_id) %>% 
  mutate(
    lag_pol = lag(date_issued, order_by = date_issued)
  ) %>% 
  ungroup() %>% 
  filter(!is.na(lag_pol)) %>% 
  mutate(interlapse = difftime(date_issued, lag_pol, units = "days") %>% 
           as.integer()) %>%
  group_by(customer_id) %>% 
  summarise(shortest_interlapse = min(interlapse, na.rm = TRUE)) %>% 
  filter(!is.na(shortest_interlapse)) %>% 
  ggplot(aes(shortest_interlapse)) + geom_density() + 
  ggtitle("What are shortest periods between policies for a client?")
```

Should we filter out specific policy statuses?

```{r}
table(ds$customer_policies$status)
```

Policy statuses vs transactions. There is only one type of transaction which is payment. So we don't care about transaction type.

```{r}
ds$policy_transactions %>% 
  ggplot(aes(transaction_amount)) + 
  geom_density() + 
  scale_x_log10()

data %>% 
  select(customer_policy_id,
         status,
         transaction_amount) %>% 
  group_by(customer_policy_id, status) %>% 
  summarize(amount = sum(transaction_amount, na.rm = TRUE)) %>% 
  na.omit() %>% 
  ggplot(aes(status, amount)) + 
  geom_boxplot() + 
  # scale_y_log10() + 
  coord_flip(ylim = c(0, 100)) + 
  labs(y = "Total policy cost, GH₵",
       title = "All policy statuses can have actual transactions")
```

It means we don't care about policy statuses now. But maybe we can drop policies with zero payments.

```{r}
data %>% 
  select(customer_policy_id,
         transaction_amount) %>% 
  group_by(customer_policy_id) %>% 
  summarise(zero_amount = sum(transaction_amount, na.rm = TRUE) == 0) %>% 
  group_by(zero_amount) %>% 
  summarise(policies = n())
```

0.4%: we don't care. Let's check a couple of case studies with very short interlapses. 

```{r}
data %>% 
  select(
    customer_id,
    customer_policy_id,
    date_issued
  ) %>% 
  distinct() %>% 
  group_by(customer_id) %>% 
  mutate(
    lag_pol = lag(date_issued, order_by = date_issued)
  ) %>% 
  ungroup() %>% 
  mutate(interlapse = difftime(date_issued, lag_pol, units = "days") %>% 
           as.integer()) %>%
  group_by(customer_id) %>% 
  mutate(tinyinterlapse = any(interlapse < 1)) %>% 
  ungroup() %>% 
  filter(tinyinterlapse) %>% 
  select(-tinyinterlapse) %>% 
  arrange(customer_id, date_issued) %>% 
  head(10)
```

The guy 164 got two policies on one day.

```{r}
data %>% 
  filter(customer_policy_id %in% c(48484, 48492)) %>% 
  select_if(~length(unique(.)) > 1)
```


Policy per crop. It makes sense. One farmer can plant different crops. Let's check similar situation for one day delay. 


```{r}
data %>% 
  select(
    customer_id,
    customer_policy_id,
    date_issued
  ) %>% 
  distinct() %>% 
  group_by(customer_id) %>% 
  mutate(
    lag_pol = lag(date_issued, order_by = date_issued)
  ) %>% 
  ungroup() %>% 
  mutate(interlapse = difftime(date_issued, lag_pol, units = "days") %>% 
           as.integer()) %>%
  group_by(customer_id) %>% 
  mutate(tinyinterlapse = any(interlapse == 1)) %>% 
  ungroup() %>% 
  filter(tinyinterlapse) %>% 
  select(-tinyinterlapse) %>% 
  arrange(customer_id, date_issued) %>% 
  head(10)
```

```{r}
data %>% 
  filter(customer_policy_id %in% c(40781, 40782)) %>% 
  select_if(~length(unique(.)) > 1)
```


Any cases when we have close dates for different seasons?


```{r}
data %>% 
  select(
    customer_id,
    customer_policy_id,
    date_issued,
    season,
    crop
  ) %>% 
  distinct() %>% 
  group_by(customer_id) %>% 
  mutate(
    lag_pol = lag(date_issued, order_by = date_issued),
    lead_pol = lead(date_issued, order_by = date_issued),
  ) %>% 
  ungroup() %>% 
  mutate(interlapse = difftime(date_issued, lag_pol, units = "days") %>% 
           as.integer(),
         interlapse_lead = difftime(lead_pol, date_issued, units = "days") %>% 
           as.integer()) %>%
  group_by(customer_id) %>% 
  mutate(has_tinyinterlapse = any(interlapse <= 31)) %>% 
  ungroup() %>% 
  filter(has_tinyinterlapse) %>% 
  select(-has_tinyinterlapse) %>% 
  group_by(customer_id) %>% 
  filter((interlapse <= 31 | interlapse_lead <= 31) &
           length(unique(season)) > 1 & 
           length(unique(crop)) == 1) %>% 
  ungroup() %T>%
  {select(., customer_id) %>% 
      distinct() %>% 
      summarize(suspicious_policies = n()) %>% 
      print()} %>% 
  arrange(customer_id, date_issued) %>% 
  head(10)
```


OK, in total we have only six clients who bought two policies for one crop with interlapse 31 days or less. We don't care about such small amount but it is better to clarify or clean it out. 

Taking into account a customer could buy two policies for two different crops in the same season we can't use unique client's policies to calculate unique purchase. A suitable approach is to count combinations of customer and season.  

We drop differences between major and minor seasons, as there are not so many policies for minor season and also taking it into account would complicate the approach.

```{r}
table(data$season_year, data$season_type)
```


```{r}
retention <- data %>% 
  select(
    customer_id,
    season_year) %>% 
  distinct() %>% 
  group_by(customer_id) %>% 
  mutate(
    first_policy = min(season_year),
    following = str_c("year_", season_year - first_policy)
  ) %>% 
  ungroup() %>% 
  filter(!is.na(first_policy),
         !is.na(following)) %>% 
  group_by(first_policy, following) %>% 
  summarise(policies = n()) %>% 
  group_by(first_policy) %>% 
  mutate(prop = policies / max(policies, na.rm = TRUE)) %>% 
  ungroup() %>% 
  complete(first_policy, following, fill = list(policies = 0L, prop = 0))

retention %>% 
  select(-prop) %>% 
  spread(following, policies)
retention %>% 
  select(-policies) %>% 
  spread(following, prop)
```


```{r}
retention %>% 
  # filter(following != "year_0") %>% 
  ggplot(aes(following, prop, fill = factor(first_policy, ordered = TRUE))) + 
  geom_bar(stat = "identity", position = "dodge") + 
  scale_y_continuous(labels = scales::percent) 
```

## Payouts

```{r}
ds$community_payouts %>% 
  sample_n(10)
```

What is `paid_by`? How many?

```{r}
ds$community_payouts$paid_by %>% unique %>% length
```

Is one of them a customer?

```{r}
1262L %in% ds$customers$customer_id
```

How many of them are not customers?

```{r}
sum(!(unique(c(ds$community_payouts$paid_by, ds$community_payouts$paid_to)) %in% 
      ds$customers$customer_id))
```

... are in the customers table?

```{r}
sum((unique(c(ds$community_payouts$paid_by, ds$community_payouts$paid_to)) %in% 
      ds$customers$customer_id))
```

Even if `paid_by/to` is a `customer_id` we can't use it now. 

## Dataset for retention analysis

We want to show influence of various factors on the retention rate. Factors are:

* client's first policy year;
* crop;
* gender;
* literacy (small number factors to be united);
* region (drop GH-TV, as one client only);
* client has phone;
* farm size (greater than 13?);
* payouts to the community before. But based on our records first payout ever was about season 2018. Probably doesn't make sense to include currently, as clients have received 2018 season payouts literally yesterday.

Let's build a set with count data on it.

```{r}
payouts_years <- ds$community_payouts %>% 
  mutate(payout_season_year = as.integer(str_extract(season, "^\\d{4}"))) %>% 
  select(
    community_id,
    payout_season_year
  ) %>% 
  distinct() %>% 
  group_by(community_id) %>% 
  mutate(payout_first_year = min(payout_season_year)) %>% 
  ungroup() %>% 
  select(-payout_season_year) 
```


```{r}
retfactors <- data %>% 
  filter(!is.na(customer_id)) %>% 
  select(
    customer_id,
    community_id,
    region,
    gender,
    literacy,
    has_phone,
    has_mobile_money,
    season_year,
    farm_size
  ) %>%
  distinct() %>%
  left_join(payouts_years, by = "community_id") %>% 
  group_by(customer_id) %>% 
  mutate(
    first_policy = min(season_year),
    retained_next_year = any(first_policy + 1 == season_year),
    retained = max(season_year) - first_policy > 0
  ) %>% 
  ungroup() %>% 
  mutate(
    big_farm = farm_size > 13,
    literacy = forcats::fct_lump(literacy, prop = .05, other_level = "other"),
    gender = forcats::fct_explicit_na(gender)
    # , has_mobile_money = forcats::fct_explicit_na(has_mobile_money),
  ) %>% 
  group_by(
    first_policy,
    region,
    community_id,
    gender,
    literacy,
    big_farm
  ) %>% 
  summarize(
    total_customers = n(),
    retained_next_year = sum(retained_next_year, na.rm = TRUE),
    retained = sum(retained, na.rm = TRUE)
  ) %>% 
  ungroup()
  
        

```


```{r}
retfactors %>% 
  group_by(first_policy) %>% 
  summarize(
    customers = sum(total_customers),
    retained_next_year = sum(retained_next_year),
    retained = sum(retained),
    prop = retained_next_year / customers
  )
```


Houston, we've had a problem here. Numbers don't match. Simplify it.

```{r}
data %>% 
  filter(!is.na(customer_id)) %>% 
  select(
    customer_id,
    season_year
  ) %>%
  distinct() %>%
  group_by(customer_id) %>% 
  summarize(
    first_policy = min(season_year),
    retained_next_year = any(first_policy + 1 == season_year),
    retained = max(season_year) - first_policy > 0
  ) %>% 
  group_by(
    first_policy
  ) %>% 
  summarize(
    total_customers = n(), # length(unique(customer_id)),
    retained_next_year = sum(retained_next_year, na.rm = TRUE),
    retained = sum(retained, na.rm = TRUE)
  ) %>% 
  ungroup()
```


OK, the problem was with duplicating customer records. So let's firstly measure individual customers and add properties afterward.

### Dataset for shiny app

```{r}
customer_retention <- data %>% 
  filter(!is.na(customer_id)) %>% 
  select(
    customer_id,
    season_year
  ) %>%
  distinct() %>%
  group_by(customer_id) %>% 
  summarize(
    first_policy = min(season_year),
    retained_next_year = any(first_policy + 1 == season_year),
    retained_after_1year = max(season_year) - first_policy > 1
  ) 
  
retfactors <- data %>% 
  filter(!is.na(customer_id)) %>% 
  select(
    customer_id,
    community_id,
    longitude,
    latitude,
    region,
    gender,
    literacy,
    farm_size,
    has_mobile_money,
    has_phone
  ) %>%
  distinct() %>%
  left_join(payouts_years, by = "community_id") %>% 
  mutate(
    farm_size = if_else(farm_size < 13, "12 acres or less", 
                        if_else(farm_size > 13, "14 acres or more", "13 acres")),
    literacy = forcats::fct_lump(literacy, prop = .05, other_level = "other"),
    gender = forcats::fct_explicit_na(gender)
    # , has_mobile_money = forcats::fct_explicit_na(has_mobile_money)
  ) 

customer_retention_factors <- 
  customer_retention %>% 
  left_join(retfactors, by = "customer_id") %>% 
  mutate(
    community_payouts_before = if_else(
      is.na(payout_first_year), 
      FALSE,
      if_else(payout_first_year <= first_policy, TRUE, FALSE)))

saveRDS(customer_retention_factors, file.path("retentionapp", "data", "customer_retention.rds"))
```


One more check. 

```{r}
customer_retention_factors %>% 
  group_by(first_policy) %>% 
  summarize(
    customers = n(),
    next_year = sum(retained_next_year),
    year2plus = sum(retained_after_1year)
  )
```


## Further developments

1. Split by crop.
2. Weather conditions.
3. Amounts of payments and payouts. 